vim.opt.backspace = "2"
vim.opt.showcmd = true
vim.opt.laststatus = 2
vim.opt.autowrite = true
vim.opt.cursorline = true
vim.opt.autoread = true
vim.opt.number = true

vim.opt.tabstop = 2
vim.opt.softtabstop = -1
vim.opt.shiftwidth = 2
vim.opt.shiftround = true
vim.opt.expandtab = true

vim.opt.textwidth = 80
vim.opt.colorcolumn = "+1"
vim.opt.mouse = ""
vim.opt.listchars = {
  eol = "\\U000f17a5",
  tab = "|=>",
  trail = "\\U000f15dc",
  multispace = "\\u2423",
  lead = " ",
  extends = "\\u25ba",
  precedes = "\\u25c0",
  nbsp = "‿",
}

vim.keymap.set("n", "<leader>h", ":set hls!<CR>")
vim.keymap.set("n", "<leader>l", ":set list!<CR>")
vim.keymap.set("n", "<leader>n", ":set number!<CR>")
vim.keymap.set("n", "<leader>nr", ":set relativenumber!<CR>")
vim.keymap.set("n", "gF", ":e <cfile><CR>")
