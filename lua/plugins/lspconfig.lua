return {
  "neovim/nvim-lspconfig",

  dependencies = {
    "williamboman/mason.nvim",
    "williamboman/mason-lspconfig.nvim",
  },

  config = function()
    local lspconfig = require("lspconfig")
    local mason = require("mason")
    local mason_lspconfig = require("mason-lspconfig")

    mason.setup()
    mason_lspconfig.setup({
      ensure_installed = {
        -- BEGIN ANSIBLE LSPS
        "lua_ls",
        "yamlls",
        "ansiblels",
        "pylsp",
        -- END ANSIBLE LSPS
      },
    })

    lspconfig.lua_ls.setup({
      settings = {
        Lua = {
          diagnostics = {
            enable = true,
            globals = { "vim" },
          },
        },
      },
    })

    -- BEGIN ANSIBLE LSP SETUPS
    lspconfig.yamlls.setup({})
    lspconfig.ansiblels.setup({})
    lspconfig.pylsp.setup({})
    -- END ANSIBLE LSP SETUPS

    vim.keymap.set("n", "K", vim.lsp.buf.hover, {})
  end,
}
