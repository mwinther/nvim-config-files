return {
  'nvim-telescope/telescope.nvim', tag = '0.1.8',
  dependencies = { 'nvim-lua/plenary.nvim' },

  keys = {
    { '<leader>tb', '<cmd>Telescope buffers<CR>' },
    { '<leader>tf', '<cmd>Telescope find_files<CR>' },
    { '<leader>tg', '<cmd>Telescope live_grep<CR>' },
    { '<leader>th', '<cmd>Telescope help_tags<CR>' },
    { '<leader>tp', '<cmd>Telescope builtin<CR>' },
    { '<leader>tr', '<cmd>Telescope registers<CR>' },
    { '<leader>tt', '<cmd>Telescope treesitter<CR>' },
  }
}
