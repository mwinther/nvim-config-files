return {
  'sainnhe/gruvbox-material',
  lazy = false,
  priority = 1000,

  config = function()
    vim.g.gruvbox_material_enable_italic = true
    vim.cmd.colorscheme('gruvbox-material')
    vim.cmd.highlight('LiteralTabs ctermbg=darkcyan guibg=darkcyan')
    vim.cmd.match('LiteralTabs /\t/')
    vim.cmd.highlight('ExtraWhitespace ctermbg=darkcyan guibg=darkcyan')
    vim.cmd.match { 'ExtraWhitespace', [[/\s\+$/]] }
  end
}
